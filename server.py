from flask import Flask
from flask_mysqldb import MySQL
from flask import request, redirect, url_for, flash
from flask import render_template

app = Flask(__name__)

# conexion a base de datos
app.config['MYSQL_HOST'] = 'localhost'
app.config["MYSQL_USER"] = "rodrigo"
app.config["MYSQL_PASSWORD"] = "newpassword"
app.config["MYSQL_DB"] = "bd"
mysql = MySQL(app)

#session
app.secret_key = 'mysecretkey'

#raiz
@app.route("/")
def index():
    return render_template('index.html')

@app.route("/register")
def register():
    return render_template('form.html')

@app.route("/access")
def access():
    return render_template("login.html")

@app.route("/intranet",methods = ['POST'])
def intranet():
    if request.method == 'POST':
        rut = request.form['Rut']
        password = request.form['Contrasena']
        cur = mysql.connection.cursor()
        cur.execute('SELECT * FROM cliente WHERE rut = {}'.format(rut))
        data = cur.fetchall()
        dataitem = data[0]
        check = dataitem[3]

        if rut == '1010' and password == check :
            return render_template("admin.html",nombre = dataitem[1],rut = dataitem[0])

        if password == check :
            return render_template("session.html",nombre = dataitem[1],rut = dataitem[0])
        else:
            flash('clave incorrecta')
            return redirect(url_for('access'))

@app.route("/reglocation/<rut>")
def reglocation(rut):
    return render_template('locationform.html',rut = rut)

@app.route("/allclient")
def allclient():
    cur = mysql.connection.cursor()
    cur.execute("""SELECT * FROM cliente""")
    rv = cur.fetchall()
    cur.close()
    return render_template('baseclients.html',clientes = rv)

@app.route("/allocation/<rut>")
def allocation(rut):
    cur = mysql.connection.cursor()
    cur.execute("""SELECT * FROM ubicacion WHERE rut_cliente = {}""".format(rut))
    rv = cur.fetchall()
    cur.close()
    return render_template('baselocations.html',ubicaciones = rv)

@app.route("/reading")
def users():
    cur = mysql.connection.cursor()
    cur.execute("""SELECT * FROM cliente""")
    rv = cur.fetchall()
    cur.close()
    return render_template('graph.html', clientes = rv)

@app.route('/addclient', methods = ['POST'])
def addclient():
    if request.method == 'POST':
        rut = request.form['Rut']
        nombre = request.form['Nombre']
        direccion = request.form['Direccion']
        password = request.form['password']

        cursor = mysql.connection.cursor()
        cursor.execute(''' INSERT INTO cliente VALUES(%s,%s,%s,%s)''',(rut, nombre, direccion, password))
        mysql.connection.commit()
        cursor.close()
        flash('Registrado Correctamente')
        return redirect(url_for('index'))

@app.route("/addlocation/<rut>", methods = ['POST'])
def addlocation(rut):
    if request.method == 'POST':
        id_ub = request.form['id_ubicacion']
        nombre = request.form['nombre_ubicacion']
        cursor = mysql.connection.cursor()
        cursor.execute(''' INSERT INTO ubicacion VALUES(%s,%s,%s)''',(id_ub, rut, nombre))
        mysql.connection.commit()
        cursor.close()
        flash('Registrada Nueva Ubicacion')
        return render_template("session.html",nombre = nombre,rut = rut)

@app.route("/modloc/<string:id>")
def modloc(id):
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM ubicacion WHERE id_ubicacion = {}'.format(id))
    data = cur.fetchall()
    dataitem = data[0]
    return render_template('locations.html',data = dataitem)

@app.route('/editclient/<string:id>')
def editclient(id):
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM cliente WHERE rut = {}'.format(id))
    data = cur.fetchall()
    dataitem = data[0]
    return render_template('clients.html',data = dataitem)

@app.route('/update/<string:id>', methods =['POST'])
def update(id):
    if request.method == 'POST':
        rut = request.form['Rut']
        nombre = request.form['Nombre']
        direccion = request.form['Direccion']
        password = request.form['password']
        cur = mysql.connection.cursor()
        cur.execute("""UPDATE cliente SET rut = %s, nombre = %s, direccion = %s, password = %s WHERE rut = %s""",(rut,nombre,direccion,password,id))
        cur.connection.commit()
        cur.close()

        flash('Datos editados Correctamente')
        return redirect(url_for('allclient'))

@app.route('/updateloc/<string:id>', methods =['POST'])
def updateloc(id):
    if request.method == 'POST':
        nombre = request.form['Nombre']
        cur = mysql.connection.cursor()
        cur.execute("""UPDATE ubicacion SET nombre_ubicacion = %s WHERE id_ubicacion = %s """,(nombre,id))
        cur.connection.commit()
        cur.close()

        flash('Datos editados Correctamente')
        return redirect(url_for('access'))


@app.route('/deleteclient/<string:id>')
def deleteclient(id):
    cur = mysql.connection.cursor()
    cur.execute("""DELETE FROM cliente WHERE rut = {}""".format(id))
    mysql.connection.commit()
    cur.close()
    flash('Cliente removido')
    return redirect(url_for('index'))

@app.route('/deleteloc/<string:id>')
def deleteloc(id):
    cur = mysql.connection.cursor()
    cur.execute("""DELETE FROM ubicacion WHERE id_ubicacion = {}""".format(id))
    mysql.connection.commit()
    cur.close()
    flash('Region removida')
    return redirect(url_for('index'))


if __name__ == "__main__":
    app.run(debug=True)
